# Maintainer: Sven-Hendrik Haase <sh@lutzhaase.com>

pkgname=lsb-release-os
pkgver=1.4
pkgrel=21
pkgdesc="LSB version query program"
arch=('any')
url="http://www.linuxbase.org/"
license=('GPL2')
depends=('bash')
replaces=('lsb-release')
conflicts=('lsb-release')
install=lsb-release.install
source=(https://downloads.sourceforge.net/lsb/${pkgname%-*}-$pkgver.tar.gz
        lsb_release_description.patch
        lsb_release_make_man_page_reproducible.patch)
md5sums=('30537ef5a01e0ca94b7b8eb6a36bb1e4'
         '72f562d8eaa8915ab85fba13e68c8d68'
         '38643eccbab543e6cfb2fa8f875e0d1e')
prepare() {
  cd "$srcdir/${pkgname%-*}-$pkgver"

  patch -Np0 < "$srcdir/lsb_release_description.patch"
  patch -Np1 < "$srcdir/lsb_release_make_man_page_reproducible.patch"
}


build() {
  cd "$srcdir/${pkgname%-*}-$pkgver"

  make
}

package() {
  cd "$srcdir/${pkgname%-*}-$pkgver"

  install -dm755 "$pkgdir/etc"
  echo "LSB_VERSION=$pkgver" >> "$pkgdir/etc/lsb-release"
  echo "DISTRIB_ID=OpenStage" >> "$pkgdir/etc/lsb-release"
  echo "DISTRIB_RELEASE=rolling" >> "$pkgdir/etc/lsb-release"
  echo -e 'CODENAME="rolling release"' >> "$pkgdir/etc/lsb-release"
  echo "DISTRIB_DESCRIPTION=\"OpenStage Linux\"" >> "$pkgdir/etc/lsb-release"

  install -Dm 644 lsb_release.1.gz "$pkgdir/usr/share/man/man1/lsb_release.1.gz"
  install -Dm 755 lsb_release "$pkgdir/usr/bin/lsb_release"
}
